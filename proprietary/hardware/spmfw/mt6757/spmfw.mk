SPM_FIRMWARE_LIST := \
  pcm_deepidle.bin \
  pcm_deepidle_by_mp1.bin \
  pcm_deepidle_by_mp1_mt6355.bin \
  pcm_deepidle_lpddr4.bin \
  pcm_deepidle_lpddr4_by_mp1.bin \
  pcm_deepidle_lpddr4_by_mp1_mt6355.bin \
  pcm_deepidle_lpddr4_mt6355.bin \
  pcm_deepidle_mt6355.bin \
  pcm_mcdi_ddrdfs.bin \
  pcm_mcdi_ddrdfs_lpddr4.bin \
  pcm_mcdi_ddrdfs_lpddr4_mt6355.bin \
  pcm_mcdi_ddrdfs_mt6355.bin \
  pcm_sodi_ddrdfs.bin \
  pcm_sodi_ddrdfs_by_mp1.bin \
  pcm_sodi_ddrdfs_by_mp1_mt6355.bin \
  pcm_sodi_ddrdfs_lpddr4.bin \
  pcm_sodi_ddrdfs_lpddr4_by_mp1.bin \
  pcm_sodi_ddrdfs_lpddr4_by_mp1_mt6355.bin \
  pcm_sodi_ddrdfs_lpddr4_mt6355.bin \
  pcm_sodi_ddrdfs_mt6355.bin \
  pcm_suspend.bin \
  pcm_suspend_by_mp1.bin \
  pcm_suspend_by_mp1_mt6355.bin \
  pcm_suspend_lpddr4.bin \
  pcm_suspend_lpddr4_by_mp1.bin \
  pcm_suspend_lpddr4_by_mp1_mt6355.bin \
  pcm_suspend_lpddr4_mt6355.bin \
  pcm_suspend_mt6355.bin \
  pcm_sodi_ddrdfs_lpddr4_2400_by_mp1_mt6355.bin \
  pcm_sodi_ddrdfs_lpddr4_2400_mt6355.bin \

REQUIRE_SPMFW_LOADER := yes
